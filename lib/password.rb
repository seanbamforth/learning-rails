require 'digest/sha2'

# This module contains functions for hashing and storing passwords
module Password
  # Generates a new salt and rehashes the password
  def Password.update(password)
    self.hash(password)
  end

  # Checks the password against the stored password
  def Password.check(password, store)
    if (password!="") and (self.hash(password) == store)
      true
    else
      false
    end
  end

  protected

  # Generates a 128 character hash
  def Password.hash(password)
    Digest::SHA512.hexdigest("#{password}:0192345AB234")
  end

end
