require 'test_helper'

class MemberMailerTest < ActionMailer::TestCase
  test "confirmation" do
    @expected.subject = 'MemberMailer#confirmation'
    @expected.body    = read_fixture('confirmation')
    @expected.date    = Time.now

    assert_equal @expected.encoded, MemberMailer.create_confirmation(@expected.date).encoded
  end

end
