class CreateActions < ActiveRecord::Migration
  def self.up
    create_table :actions do |t|
      t.string :guid
      t.string :email
      t.string :action

      t.timestamps
    end
  end

  def self.down
    drop_table :actions
  end
end
