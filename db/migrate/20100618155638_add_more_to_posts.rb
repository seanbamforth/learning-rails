class AddMoreToPosts < ActiveRecord::Migration
  def self.up
    add_column :posts, :publishTime, :time
    add_column :posts, :By, :string, :limit => 30, :null => false, :default => "sean"
    Post.create :title => "Third!"
  end

  def self.down
    remove_column :posts, :By
    remove_column :posts, :publishTime
  end
end
