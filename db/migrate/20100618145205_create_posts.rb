class CreatePosts < ActiveRecord::Migration
  def self.up
    create_table :posts do |t|
      t.string :title
      t.text :body

      t.timestamps
    end
    Post.create :title => "Initial migration Ho!"
  end

  def self.down
    drop_table :posts
  end
end
