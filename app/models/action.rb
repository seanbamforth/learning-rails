class Action < ActiveRecord::Base

  def Action.findguid(guid,type)
    action = Action.find(:first, :conditions => ['guid = ? and action = ?',guid,type])
    action.email if action
  end
  
  def Action.register_action(email,type)

    chars = ("a".."z").to_a + ("0".."9").to_a
    newguid = ""
    1.upto(40) { |i| newguid << chars[rand(chars.size-1)] }

    @action = Action.new
    @action.guid = newguid
    @action.email = email 
    @action.action = type 

    @action.guid if @action.save 
  end
  
end
