class MemberMailer < ActionMailer::Base
  
  def registration_url(guid)
    url_for :controller => 'user', :action => 'registeremail', :guid => guid, :only_path => false
  end
  
  def confirmation(user,guid)
    subject    ('Hello ' + user.name)
    recipients 'sean@select-cs.co.uk'
    from       'sean@select-cs.co.uk'
    body       :theuser => user,:registerurl => registration_url (guid)
  end

end
