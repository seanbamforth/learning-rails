class User < ActiveRecord::Base
 
  # Checks login information
  def self.authenticate(email, pass)
    user = find(:first, :conditions => ['email = ? and registered = ?',email,true])

    if not user
      return false  
    end
    
    if Password::check(pass,user.password)
      user
    else
      return false
    end
  end

  def self.exists(email) 
    user = find(:first, :conditions => ['email = ?',email])
    if user
      true 
    else 
      false
    end 
  end
  
  protected

  def before_save
    self.password = Password::update(self.password) if self.password_changed?
  end
 
end
