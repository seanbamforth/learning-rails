class UserController < ApplicationController
  
  def authenticate
     @User = User.new(params[:userform])
     
     valid_user = User.authenticate(@User.email,@User.password)
     
     if valid_user 
       session[:user_email]=valid_user.email
       session[:user_id]=valid_user.id
       session[:user]=valid_user
       session[:logged_in] = true
       redirect_to :action => 'private'
     else 
        flash[:notice] = "Invalid User/Password"
        redirect_to :action=> 'login'
     end  
  end
  
  def createnew
    @User = User.new(params[:userform])
    
    #if User.exists(@User.email)
    #  flash[:notice] = "This user already exists. If you've forgotten the password, we can send it to you."
    #  redirect_to :action=> 'register'
    #  return 
    #end
    
    @User.registered = false
    if (@User.save)
      guid = Action.register_action(@User.email,"REGISTER")
      MemberMailer.deliver_confirmation(@User,guid)
    end
  end
  
  def logout
    if session[:user_id]
      reset_session
      redirect_to :action=> 'login'
    end
  end

  def login
  end
  
  def registeremail
    @email=Action.findguid(params[:guid],"REGISTER")
    
    flash[:notice] = ""

    if @email
      user = User.find(:first, :conditions => ['email = ?',@email])

      if user 
        user.registered=true
        user.save
      else 
        flash[:notice] = "user not found"
      end
    else 
      flash[:notice] = "email not found [#{@email} ], "
    end
      
  end
  
  def register
  end
  
  def private
    if !session[:user_id]
        reset_session
        redirect_to :action=> 'login'
    end
  end

end
